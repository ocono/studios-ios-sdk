#
#  Be sure to run `pod spec lint weqsdk.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|
  spec.name            = "WeqSdk"
  spec.version         = "0.2.0"
  spec.summary         = "WeQ Studios Analytics SDK"
  spec.description     = <<-DESC
      SDK provides API for the game developer to invoke events when important things happening in the game. Which could be used to analyse interactions, collect user device data for additional statistics.
                   DESC
  spec.homepage        = "https://bitbucket.org/ocono/studios-ios-sdk"
  spec.license         = { :type => "MIT", :file => "WeqSdk/licence" }
  spec.author          = "WeQ Studios"
  spec.platform        = :ios, "10.0"
  spec.swift_version   = ["4.2"]
  spec.swift_versions  = ["4.2", "5.0"]
  spec.source          = { :git => "https://bitbucket.org/ocono/studios-ios-sdk.git", :tag => "#{spec.version}" }
  spec.module_name     = "WeqSdk"
  spec.default_subspec = "WeqSdk"

  spec.subspec 'WeqSdk' do |sdk|
    sdk.source_files   = 'WeqSdk/WeqSdk/**/*.{h,m.,swift}'
    sdk.resources    = 'WeqSdk/WeqSdk/Sdk.plist'
    sdk.dependency 'GzipSwift'
  end
end
