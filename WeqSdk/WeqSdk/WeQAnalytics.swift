import Foundation
import UIKit
import AdSupport
import os.log

@objc public class WeqAnalytics: NSObject {
    
    private static var eventRepository: EventRepository?
    private static var parameterStoreRepository: ParameterStoreRepository?
    private static var collector: EventCollector?
    private static var trackerState: TrackerState?
    private static var isInitialized: Bool = false
    private static var urlSession: URLSession = URLSession.shared
    private static var apiQueue: DispatchQueue = DispatchQueue(label: "weq-api");
    
    public static func configure(config: WeQConfig) {
        self.configure(config: config, sdkConfig: SdkConfiguration())
    }
    
    public static func configure(config: WeQConfig, sdkConfig: SdkConfiguration) {
        if !config.isAppTokenValid() {
            Logger.debug("App token is not set")
            return;
        }
        if !config.isGameIdValid() {
            Logger.debug("Store game id not set")
            return;
        }
        
        apiQueue.async(execute: {
            if (self.isInitialized) {
                Logger.debug("Already initialized.")
                return;
            }
            
            self.trackerState = TrackerState(config: config, sdkConfig: sdkConfig, session: Session())
            self.collector = EventCollector(trackerState: self.trackerState!)
            
            let httpApiReader = HttpApiReader(trackerState: self.trackerState!, urlSession: urlSession)
            let database = AnalyticsDatabaseFactory.create(dropBeforeCreating: false, databasePath: sdkConfig.getDatabasePath())
            
            let gameUsersTable = GameUsersTable(database: database)
            let parameterValuesTable = ParameterValuesTable(database: database)
            let eventsTable = EventsTable(database: database);
            
            self.eventRepository = EventRepository(trackerState: self.trackerState!,eventsTable: eventsTable)
            self.parameterStoreRepository = ParameterStoreRepository(parameterValueTable: parameterValuesTable, trackerState: self.trackerState!)
            
            GameUserResolver(
                gameUsersTable: gameUsersTable,
                trackerState: self.trackerState!,
                trackingEventCollector: self.collector!,
                httpApiReader: httpApiReader
            ).resolve()
            
            ParameterStoreUpdater(
                parameterStoreRepository: self.parameterStoreRepository!,
                trackerState: self.trackerState!,
                httpApiReader: httpApiReader
            ).start()
            
            EventPublisher(
                repository: eventRepository!,
                trackerState: self.trackerState!,
                httpApiReader: httpApiReader
            ).start()
            
            self.isInitialized = true;
        })
    }
    
    public static func event(event: WeQEvent) {
        guard let state = self.trackerState else {
            Logger.debug("Tracker state is not initialized")
            return
        }
        
        if !state.isTrackingEnabled() {
            return
        }
        
        if (self.isInitialized == false) {
            Logger.debug("Tracker is not initialized")
            return
        }
        
        guard let collector = self.collector else {
            Logger.debug("Collector not initialized")
            return
        }
        
        apiQueue.async(execute: {
            eventRepository!.addEvent(
                event: collector.createTrackingEvent(weqEvent: event)
            )
        })
    }
    
    @objc public static func configure(appToken: String, gameId: String) {
        self.configure(config: WeQConfig(appToken: appToken, gameId: gameId))
    }
    
    @objc public static func event(
        category: String,
        name: String,
        tags: Array<String>,
        value1: String,
        value2: String,
        value3: String,
        value4: String,
        value5: String
    ) {
        
        self.event(
            event: WeQEvent(
                definition: EventDefinition(category: category, name: name, tags: EventCoder.unserializeTags(tags: tags)),
                value: EventValue(
                    value1: EventCoder.unserializeValue(value: value1),
                    value2: EventCoder.unserializeValue(value: value2),
                    value3: EventCoder.unserializeValue(value: value3),
                    value4: EventCoder.unserializeValue(value: value4),
                    value5: EventCoder.unserializeValue(value: value5)
                )
            )
        )
    }
    
    @objc public static func disableTracking() {
        guard let state = self.trackerState else {
            Logger.debug("Tracker not initialized.")
            return
        }
        
        state.setTrackingEnabled(enabled: false)
        Logger.debug("Tracking disabled")
    }
    
    @objc public static func enableTracking() {
        guard let state = self.trackerState else {
            Logger.debug("Tracker not initialized.")
            return
        }
        
        state.setTrackingEnabled(enabled: true)
        Logger.debug("Tracking resumed")
    }
    
    @objc public static func debug(enable: Bool) {
        Logger.setIsEnabled(enabled: enable)
    }
    
    @objc public static func registerParameter(name: String, type: String, defaultValue: String, description: String) {
        parameterStoreRepository?.registerParameterValue(
            parameterValue: ParameterValue(
                name: name,
                type: type,
                description: description,
                defaultValue: defaultValue
            )
        )
    }
    
    @objc public static func getParameter(name: String) -> String? {
        return parameterStoreRepository?.getParameterValue(name: name)
    }
    
    public static func setUrlSession(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    public static func setInitialized(initialized: Bool) {
        self.isInitialized = initialized
    }
}
