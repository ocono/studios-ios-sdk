//
//  WeqSdk.h
//  WeqSdk
//
//  Created by Aurimas Lickus on 06.11.19.
//  Copyright © 2019 Aurimas Lickus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Project version number for WeqSdk.
FOUNDATION_EXPORT double WeqSdkVersionNumber;

//! Project version string for WeqSdk.
FOUNDATION_EXPORT const unsigned char WeqSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WeqSdk/PublicHeader.h>


