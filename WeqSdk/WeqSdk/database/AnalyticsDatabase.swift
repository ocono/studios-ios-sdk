import Foundation

import SQLite3
import os.log

public class AnalyticsDatabase {
    
    private var db: OpaquePointer?
    
    convenience init(dbPath: String, createTablesSQL:[String], dropTablesSQL: [String]) {
        self.init(
            dbPath: dbPath,
            createTablesSQL: createTablesSQL,
            dropTablesSQL: dropTablesSQL,
            dropDatabase: false
        )
    }
    
    init(dbPath: String, createTablesSQL:[String], dropTablesSQL: [String], dropDatabase: Bool) {
        self.db = openDatabase(dbPath: dbPath)
        if (dropDatabase) {
            for dropSQL in dropTablesSQL {
                dropTable(table: dropSQL)
            }
        }
        
        for createSQL in createTablesSQL {
            createTable(table: createSQL)
        }
    }
    
    public func getConnection() -> OpaquePointer? {
        return db;
    }
    
    private func dropTable(table: String) {
        var dropEventsTableSQL: OpaquePointer? = nil
        defer {
            sqlite3_finalize(dropEventsTableSQL)
        }
        if sqlite3_prepare_v2(db, table, -1, &dropEventsTableSQL, nil) == SQLITE_OK {
            if sqlite3_step(dropEventsTableSQL) == SQLITE_DONE {
                Logger.debug("Table deleted")
            } else {
                Logger.debug("Failed to delete table")
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(dropEventsTableSQL))
            Logger.debug(String(format:"CREATE TABLE statement could not be prepared %@", sqlError))
        }
    }
    
    private func createTable(table: String) {
        var createTableStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(createTableStatement)
        }
        if sqlite3_prepare_v2(db, table, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                Logger.debug(String(format:"Table '%@' created", table))
            } else {
                let sqlError = String(cString: sqlite3_errmsg(db))
                Logger.debug(String(format:"Table '%@' could not be created %@", table, sqlError))
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(db))
            Logger.debug(String(format:"CREATE TABLE statement could not be prepared %@", table, sqlError))
        }
    }
    
    private func openDatabase(dbPath: String) -> OpaquePointer? {
        var db: OpaquePointer? = nil
        if sqlite3_open_v2(dbPath, &db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, nil) == SQLITE_OK {
            Logger.debug(String(format:"Successfully opened connection to database at %@", dbPath))
            return db
        } else {
            let sqlError = String(cString: sqlite3_errmsg(db))
            Logger.debug(String(format:"Unable to open database %@", sqlError))
        }
        
        return db
    }
}
