import Foundation

public class AnalyticsDatabaseFactory {
    
    public static func create(dropBeforeCreating: Bool, databasePath: String) -> AnalyticsDatabase {
        return AnalyticsDatabase(
            dbPath: databasePath,
            createTablesSQL: [
                EventsTable.createEventsTableSQL,
                ParameterValuesTable.createParameterValuesTableSQL,
                GameUsersTable.createUsersTableSQL
            ],
            dropTablesSQL: [
                EventsTable.dropEventsTableSQL,
                ParameterValuesTable.dropParameterValuesTableSQL,
                GameUsersTable.dropUsersTableSQL
            ],
            dropDatabase: dropBeforeCreating
        )
    }
}
