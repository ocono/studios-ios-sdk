
import Foundation

import SQLite3
import os.log

public class ParameterValuesTable {
    static let createParameterValuesTableSQL = """
                                           CREATE TABLE IF NOT EXISTS parameter_values(
                                           name TEXT NOT NULL UNIQUE,
                                           attributes TEXT,
                                           timestamp INTEGER);
                                       """
    static let dropParameterValuesTableSQL = "DROP TABLE parameter_values;"
    private let replaceParameterValuesSQL = "INSERT OR REPLACE INTO parameter_values (name, attributes, timestamp) VALUES (?, ?, ?);"
    private let queryAllParameterValuesSQL = "SELECT * FROM parameter_values;"
    private let deleteParameterValuesByKeySQL = "DELETE FROM parameter_values WHERE name = ?;"
    private let analyticsDatabase: AnalyticsDatabase;
    
    init(database: AnalyticsDatabase) {
        self.analyticsDatabase = database;
    }

    public func upsert(parameterValue: ParameterValue) -> Bool {
       guard let attributes = JsonEncoder.encode(parameterValue) else {
           return false;
       }

       var replaceStatement: OpaquePointer? = nil
       defer {
           sqlite3_finalize(replaceStatement)
       }
       if sqlite3_prepare_v2(analyticsDatabase.getConnection(), replaceParameterValuesSQL, -1, &replaceStatement, nil) == SQLITE_OK {
          sqlite3_bind_text(replaceStatement, 1, (parameterValue.name as NSString).utf8String, -1, nil)
          sqlite3_bind_text(replaceStatement, 2, (attributes as NSString).utf8String, -1, nil)
          sqlite3_bind_int64(replaceStatement, 3, DateTime.currentTimeMillis())
          if sqlite3_step(replaceStatement) == SQLITE_DONE {
              return true;
          } else {
              let sqlError = String(cString: sqlite3_errmsg(replaceStatement))
              Logger.debug(String(format:"Cannot save event %@", sqlError))
          }
       } else {
          let sqlError = String(cString: sqlite3_errmsg(replaceStatement))
          Logger.debug(String(format:"INSERT statement could not be prepared %@", sqlError))
       }
       return false;
    }
    
    public func findAll() -> [String: ParameterValue] {
        var queryStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(queryStatement)
        }
        var parameterValues = [String: ParameterValue]()
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), queryAllParameterValuesSQL, -1, &queryStatement, nil) == SQLITE_OK {
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                let name = String(cString: sqlite3_column_text(queryStatement, 0))
                let attributes = String(cString: sqlite3_column_text(queryStatement, 1))
                guard let event = JsonEncoder.decode(ParameterValue.self, from: attributes) else {
                   continue;
                }
                parameterValues[name] = event
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(queryStatement))
            Logger.debug(String(format:"SELECT statement could not be prepared %@", sqlError))
        }

        return parameterValues
    }
    
    public func deleteByParameterName(name: String) -> Int32 {
         var deleteStatement: OpaquePointer? = nil
         defer {
             sqlite3_finalize(deleteStatement)
         }
         if sqlite3_prepare_v2(analyticsDatabase.getConnection(), deleteParameterValuesByKeySQL, -1, &deleteStatement, nil) == SQLITE_OK {
             sqlite3_bind_text(deleteStatement, 1, (name as NSString).utf8String, -1, nil)
             if sqlite3_step(deleteStatement) == SQLITE_DONE {
                 return sqlite3_changes(analyticsDatabase.getConnection())
             }
         } else {
             let sqlError = String(cString: sqlite3_errmsg(deleteStatement))
             Logger.debug(String(format:"DELETE statement could not be prepared %@", sqlError))
         }

         return 0
    }
}
