import Foundation

public class ParameterStoreUpdater {
    
    private let workerQueue: DispatchQueue = DispatchQueue(label: "weq-parameter-store-updater")
    private let timer: DispatchSourceTimer
    private let parameterStoreRepository: ParameterStoreRepository
    private let trackerState: TrackerState
    private let httpApiReader: HttpApiReader
    
    init(parameterStoreRepository: ParameterStoreRepository, trackerState: TrackerState, httpApiReader: HttpApiReader) {
        self.timer = DispatchSource.makeTimerSource(queue: workerQueue)
        self.parameterStoreRepository = parameterStoreRepository
        self.trackerState = trackerState
        self.httpApiReader = httpApiReader
        
        timer.schedule(deadline: .now(), repeating: .seconds(trackerState.sdkConfig.getParameterStoreUpdateFrequencyInSeconds()))
    }
    
    public func start() -> Void {
        Logger.debug("Parameter store updater started")
        timer.setEventHandler {
            if self.trackerState.session.userId == nil {
                return
            }
            
            if (!UtilNetwork.isConnected()) {
                return
            }
            
            let parameterStoreRequest = ParameterStoreRequest(parameters: Array(self.parameterStoreRepository.getCache().values))
            if (parameterStoreRequest.parameters.isEmpty) {
                return
            }
            
            guard let jsonRequest = JsonEncoder.encode(parameterStoreRequest) else {
                return
            }
            
            let requestUri = String(format:"/game/%@/user/%@/parameter-store", self.trackerState.weqConfig.gameId, self.trackerState.session.userId!)
            self.httpApiReader.sendPost(jsonRequest: jsonRequest, requestUri: requestUri) { jsonResponse in
                self.processApiResponse(jsonResponse: jsonResponse)
            }
        }
        timer.resume()
    }
    
    private func processApiResponse(jsonResponse: String) {
        guard let parameterStoreResponse = JsonEncoder.decode(ParameterStoreResponse.self, from: jsonResponse) else {
            Logger.debug("Cannot unserialize response from parameter store")
            return
        }
        
        if parameterStoreResponse.parameters.isEmpty {
            return
        }
        
        let parameterCache = self.parameterStoreRepository.getCache()
        for parameter in parameterStoreResponse.parameters {
            if !parameterCache.keys.contains(parameter.name) {
                continue
            }
            
            let parameterValue = parameterCache[parameter.name]
            parameterValue?.value = parameter.value
            
            if self.parameterStoreRepository.upsert(parameterValue: parameterValue!) == false {
                Logger.debug("Failed to save parameter: " + parameter.name)
            }
        }
        
        self.parameterStoreRepository.refreshCache()
    }
}
