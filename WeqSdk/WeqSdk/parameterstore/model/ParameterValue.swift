import Foundation

public class ParameterValue: Codable {
    
    let name: String;
    let type: String
    let description: String;
    let defaultValue: String;
    var value: String?;
    
    public init(name: String, type: String, description: String, defaultValue: String) {
        self.name = name
        self.type = type
        self.description = description
        self.defaultValue = defaultValue
        self.value = nil
    }
    
    public init(name: String, type: String, description: String, defaultValue: String, value: String) {
         self.name = name
         self.type = type
         self.description = description
         self.defaultValue = defaultValue
         self.value = value
     }
    
    public func setValue(value: String) -> Void {
        self.value = value
    }
}
