import Foundation

public class ParameterStoreResponse : Codable {
    
    let parameters: [ParameterStoreResponseValue]
    
    public init(parameters: [ParameterStoreResponseValue]) {
        self.parameters = parameters
    }
}
