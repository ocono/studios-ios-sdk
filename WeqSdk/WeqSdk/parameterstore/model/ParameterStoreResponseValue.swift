import Foundation

public class ParameterStoreResponseValue : Codable {
   
    let name: String
    let value: String
    
    public init(name: String, value: String) {
        self.name = name
        self.value = value
    }
}
