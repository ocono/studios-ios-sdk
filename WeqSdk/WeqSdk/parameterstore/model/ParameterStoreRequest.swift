import Foundation

public class ParameterStoreRequest: Codable {
    
    let parameters: [ParameterValue]
    
    public init(parameters: [ParameterValue]) {
        self.parameters = parameters
    }
}
