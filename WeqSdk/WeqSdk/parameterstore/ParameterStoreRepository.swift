
import Foundation

class ParameterStoreRepository {
    
    private let parameterValueTable: ParameterValuesTable
    private let workerQueue: DispatchQueue = DispatchQueue(label: "weq-parameter-store-repository")
    private let trackerState: TrackerState
    private var parameterCache = [String:ParameterValue]()
    
    init(parameterValueTable: ParameterValuesTable, trackerState: TrackerState) {
        self.parameterValueTable = parameterValueTable
        self.trackerState = trackerState
        
        refreshCache()
    }
    
    public func registerParameterValue(parameterValue: ParameterValue) -> Void {
        if !trackerState.isTrackingEnabled() || parameterValue.name.isEmpty {
            return
        }
        
        workerQueue.async {
            if self.parameterCache.keys.contains(parameterValue.name) && self.parameterCache[parameterValue.name]!.value != nil {
                parameterValue.setValue(value: self.parameterCache[parameterValue.name]!.value!)
            }
            
            if self.parameterValueTable.upsert(parameterValue: parameterValue) == false {
                Logger.debug("Parameter value could not be saved to local storage")
                return
            }
            
            self.parameterCache[parameterValue.name] = parameterValue
        }
    }
    
    public func getParameterValue(name: String) -> String? {
        return workerQueue.sync {
            if !parameterCache.keys.contains(name) {
                return nil
            }
            
            let parameterValue = parameterCache[name];
            if parameterValue == nil {
                return nil
            }
            
            return parameterValue?.value != nil ? parameterValue?.value : parameterValue?.defaultValue
        }
    }
    
    public func refreshCache() -> Void {
        workerQueue.async {
            self.parameterCache.removeAll()
            self.parameterCache = self.parameterValueTable.findAll();
        }
    }
    
    public func getCache() -> [String:ParameterValue] {
        parameterCache
    }
    
    public func upsert(parameterValue: ParameterValue) -> Bool {
        return parameterValueTable.upsert(parameterValue: parameterValue)
    }
}

