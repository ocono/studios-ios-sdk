import Foundation

public class SdkConfiguration {
    
    private let applicationConfig: NSDictionary
    
    init(applicationConfig: NSDictionary) {
        self.applicationConfig = applicationConfig
    }
    
    init() {
        let path: String = Bundle(for: SdkConfiguration.self).url(forResource: "Sdk", withExtension: "plist")?.path ?? ""
        self.applicationConfig = NSDictionary(contentsOfFile: path)!
    }
    
    public func getEventIngressHostname() -> String {
        applicationConfig.object(forKey: "WEQ_SDK_EVENT_INGRESS_HOSTNAME") as! String
    }
    
    public func getEventFlushFrequencyInSeconds() -> Int {
        guard let eventFlushFrequency = applicationConfig.object(forKey: "WEQ_SDK_EVENT_FLUSH_FREQUENCY_IN_SECONDS") else {
            return 60
        }
        
        return Int(eventFlushFrequency as! String)!
    }
    
    public func getEventFlushBatchSize() -> Int {
        guard let eventFlushBatchSize = applicationConfig.object(forKey: "WEQ_SDK_EVENT_FLUSH_BATCH_SIZE") else {
            return 50
        }
        
        return Int(eventFlushBatchSize as! String)!
    }
    
    public func getEventStorageSizeLimit() -> Int {
        guard let eventStorageSizeLimit = applicationConfig.object(forKey: "WEQ_SDK_EVENT_STORAGE_SIZE_LIMIT") else {
            return 5000
        }
        
        return Int(eventStorageSizeLimit as! String)!
    }
    
    public func getEventStorageDeleteOldest() -> Int {
        guard let eventStorageDeleteOldest = applicationConfig.object(forKey: "WEQ_SDK_EVENT_STORAGE_DELETE_OLDEST") else {
            return 50
        }
        
        return Int(eventStorageDeleteOldest as! String)!
    }
    
    public func getDatabasePath() -> String {
        guard let path = applicationConfig.object(forKey: "WEQ_SDK_EVENT_DATABASE_PATH") as! String? else {
            return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("analytics.sqlite").path
        }
        return path;
    }
    
    public func getParameterStoreUpdateFrequencyInSeconds() -> Int {
        guard let eventFlushFrequency = applicationConfig.object(forKey: "WEQ_SDK_PARAMETER_STORE_UPDATE_FREQUENCY_IN_SECONDS") else {
            return 60
        }
        
        return Int(eventFlushFrequency as! String)!
    }
}
