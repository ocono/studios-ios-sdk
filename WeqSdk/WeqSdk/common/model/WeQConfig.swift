import Foundation

@objc public class WeQConfig: NSObject {
    let appToken: String
    let gameId: String
    
    @objc public init(appToken: String, gameId: String) {
        self.appToken = appToken
        self.gameId = gameId
    }
    
    public func isAppTokenValid() -> Bool {
        !appToken.isEmpty
    }
    
    public func isGameIdValid() -> Bool {
        !gameId.isEmpty
    }
}
