
import Foundation
import os.log
import Gzip

public class GameUserResolver {
    
    private let gameUsersTable: GameUsersTable
    private let trackerState: TrackerState
    private let trackingEventCollector: EventCollector
    private let httpApiReader: HttpApiReader;
    private var numberOfAttempts: Int8 = 0
    private var allowedNumberOfAttemps: Int8 = 5
    private var gameUserResolutionAttemptFrequencyInSeconds = 10
    private let timer: DispatchSourceTimer = DispatchSource.makeTimerSource(queue: DispatchQueue(label: "weq-game-user-resolver"))
    
    init(
        gameUsersTable: GameUsersTable,
        trackerState: TrackerState,
        trackingEventCollector: EventCollector,
        httpApiReader: HttpApiReader
    ) {
        self.gameUsersTable = gameUsersTable
        self.trackerState = trackerState
        self.trackingEventCollector = trackingEventCollector
        self.httpApiReader = httpApiReader
        
        timer.schedule(deadline: .now(), repeating: .seconds(gameUserResolutionAttemptFrequencyInSeconds))
    }
    
    public func resolve() -> Void {
        Logger.debug("Resolving game user")
        timer.setEventHandler {
            if self.shouldRun() == false {
                self.timer.cancel()
            }
            
            let gameUser = self.gameUsersTable.findOne()
            if gameUser != nil {
                self.trackerState.session.userId = gameUser!.id
                return
            }
            
            if !UtilNetwork.isConnected() {
                return
            }
            
            let gameUserResponse = self.fetchGameUserFromAPI()
            if (gameUserResponse != nil && gameUserResponse?.userId != nil) {
                if self.gameUsersTable.upsert(user: GameUser(id: gameUserResponse!.userId)) == true {
                    self.trackerState.session.userId = gameUserResponse!.userId
                }
            }
            self.numberOfAttempts += 1
        }
        timer.resume()
    }
    
    private func shouldRun() -> Bool {
        if trackerState.session.userId != nil {
            Logger.debug("Game user was assigned")
            return false
        }
        
        if (numberOfAttempts >= allowedNumberOfAttemps) {
            Logger.debug("Failed to get game user after number of retries")
            return false
        }
        
        return true
    }
    
    private func fetchGameUserFromAPI() -> GameUserResponse? {
        let gameUserRequest = GameUserRequest(
            deviceInfo: self.trackingEventCollector.createTrackingDeviceInfo()
        )
        
        guard let jsonRequest = JsonEncoder.encode(gameUserRequest) else {
            return nil
        }
        
        let requestUri = String(format:"/game/%@/user", self.trackerState.weqConfig.gameId)
        var matchedGameUserResponse: GameUserResponse? = nil
        httpApiReader.sendPostBlocking(
            jsonRequest: jsonRequest,
            requestUri: requestUri,
            successHandler: { jsonResponse in
                guard let gameUserResponse = JsonEncoder.decode(GameUserResponse.self, from: jsonResponse) else {
                    Logger.debug("Cannot unserialize response from game user API endpoint")
                    return
                }
                matchedGameUserResponse = gameUserResponse
        },
            errorHandler: {
                Logger.debug("Failed to retrieve game user")
        })
        return matchedGameUserResponse
    }
}
