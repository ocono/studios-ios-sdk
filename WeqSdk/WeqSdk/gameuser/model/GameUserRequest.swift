import Foundation

public class GameUserRequest:  Codable {
    
    let deviceInfo: TrackingDeviceInfo
    
    init(deviceInfo: TrackingDeviceInfo) {
        self.deviceInfo = deviceInfo
    }
}
