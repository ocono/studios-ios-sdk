import Foundation

public class GameUserResponse: Codable {
   
    let advertiserDeviceId: String
    let userId: String
    
    init(advertiserDeviceId: String, userId: String) {
        self.advertiserDeviceId = advertiserDeviceId
        self.userId = userId
    }
}
