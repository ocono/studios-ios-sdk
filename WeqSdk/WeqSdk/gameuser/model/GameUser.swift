
import Foundation

public class GameUser: Codable {
    let id: String
    
    init(id: String) {
        self.id = id
    }
}
