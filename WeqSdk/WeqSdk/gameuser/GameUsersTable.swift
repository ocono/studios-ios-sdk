import Foundation

import SQLite3
import os.log

class GameUsersTable {
    
    static let createUsersTableSQL = """
                                           CREATE TABLE IF NOT EXISTS game_users(
                                           id TEXT NOT NULL UNIQUE,
                                           timestamp INTEGER);
                                       """
    static let dropUsersTableSQL = "DROP TABLE game_users;"
    private let insertUserSQL = "INSERT OR REPLACE INTO game_users (id, timestamp) VALUES (?, ?);"
    private let queryUserSQL = "SELECT * FROM game_users ORDER BY timestamp DESC LIMIT 1;"
    
    private let analyticsDatabase: AnalyticsDatabase;
    
    init(database: AnalyticsDatabase) {
        self.analyticsDatabase = database;
    }
    
    public func upsert(user: GameUser) -> Bool {
        guard let attributes = JsonEncoder.encode(user) else {
            return false;
        }
        
        var insertStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(insertStatement)
        }
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), insertUserSQL, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (user.id as NSString).utf8String, -1, nil)
            sqlite3_bind_int64(insertStatement, 2, DateTime.currentTimeMillis())
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                Logger.debug("User successfully saved")
                return true;
            } else {
                let sqlError = String(cString: sqlite3_errmsg(insertStatement))
                Logger.debug(String(format:"Failed to save user %@", sqlError))
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(insertStatement))
            Logger.debug(String(format:"INSERT statement could not be prepared %@", sqlError))
        }
        return false;
    }
    
    public func findOne() -> GameUser? {
        var queryStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(queryStatement)
        }
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), queryUserSQL, -1, &queryStatement, nil) == SQLITE_OK {
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                let id = String(cString: sqlite3_column_text(queryStatement, 0))
                return GameUser(id: id)
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(queryStatement))
            Logger.debug(String(format:"SELECT statement could not be prepared %@", sqlError))
        }
        
        return nil
    }
    
}
