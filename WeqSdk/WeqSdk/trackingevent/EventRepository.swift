import Foundation
import SQLite3
import os.log

class EventRepository {
    private let workerQueue: DispatchQueue = DispatchQueue(label: "weq-event-repository")
    private let sdkConfig: SdkConfiguration
    private var numberOfEventsInStorage: Int32 = 0
    private let trackerState: TrackerState
    private let eventsTable: EventsTable
    
    init(trackerState: TrackerState, eventsTable: EventsTable) {
        self.trackerState = trackerState
        self.sdkConfig = trackerState.sdkConfig
        self.numberOfEventsInStorage = eventsTable.countEvents()
        self.eventsTable = eventsTable
    }

    public func addEvent(event: TrackingEvent) {
        workerQueue.async {
            if (self.numberOfEventsInStorage >= self.sdkConfig.getEventStorageSizeLimit()) {
                Logger.debug(String(format:"Reached limit of events that can be stored to internal database. Clearing '%d' oldest events", self.sdkConfig.getEventStorageSizeLimit()));
                self.clearOldEvents()
            }

            if (self.eventsTable.insertEvent(event: event)) {
                self.incrementEventCounter()
            }
        };
    }

    public func getEvents(limit: Int) -> [String: TrackingEvent] {
        eventsTable.getEvents(limit: limit)
    }

    public func clearByKeys(keys: [String]) {
        workerQueue.async {
            let clearedKeys = self.eventsTable.clearByKeys(keys: keys)
            if (clearedKeys > 0) {
                self.numberOfEventsInStorage = self.eventsTable.countEvents()
            }
        }
    }

    private func clearOldEvents() {
        let count = eventsTable.clearOldEvents(numberOfOldEvents: sdkConfig.getEventStorageDeleteOldest())
        if (count > 0) {
            self.numberOfEventsInStorage = eventsTable.countEvents()
            Logger.debug(String(format:"Successfully deleted '%d' events", count))
        }
    }

    private func incrementEventCounter() {
        if (numberOfEventsInStorage == 0) {
            numberOfEventsInStorage = eventsTable.countEvents()
            return;
        }
        numberOfEventsInStorage += 1
        Logger.debug(String(format:"Number of events in storage '%d'", numberOfEventsInStorage))
    }
}
