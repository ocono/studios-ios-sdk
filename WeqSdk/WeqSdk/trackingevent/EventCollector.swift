import Foundation
import UIKit
import AdSupport
import CoreTelephony

class EventCollector {
    
    private static var trackingAppInfo = EventCollector.createTrackingAppInfo()
    private var advertisingDeviceId: String
    private let config: WeQConfig
    private let jailBroken = EventCollector.isJailBroken()
    private let session: Session
    
    init(trackerState: TrackerState) {
        self.config = trackerState.weqConfig
        self.session = trackerState.session
        self.advertisingDeviceId = "";
        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            self.advertisingDeviceId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        }
    }
    
    public func createTrackingEvent(weqEvent: WeQEvent) -> TrackingEvent {
        TrackingEvent(
            id: UUID().uuidString,
            session: createSession(),
            event: weqEvent,
            time: TrackingTime(clientTime: DateTime.now()),
            gameId: config.gameId,
            appInfo: EventCollector.trackingAppInfo,
            deviceInfo: createTrackingDeviceInfo(),
            connection: createTrackingConnection()
        )
    }
    
    private func createSession() -> TrackingSession {
        TrackingSession(
            id: session.sessionId,
            advertisingDeviceId: advertisingDeviceId,
            gameUserId: self.session.userId ?? "",
            startTime: session.start,
            currentTime: DateTime.now(),
            deviceId: "",
            gameCenterId: ""
        )
    }
    
    private static func createTrackingAppInfo() -> TrackingAppInfo {
        let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? ""
        
        return TrackingAppInfo(
            appId: bundleIdentifier,
            build: buildVersion,
            version: version,
            signature: ""
        )
    }
    
    private func createTrackingIphoneInfo() -> TrackingIphoneInfo {
        let device = UIDevice.current
        return TrackingIphoneInfo(
            isMultitaskingSupported: device.isMultitaskingSupported,
            name: device.name,
            systemName: device.systemName,
            systemVersion: device.systemVersion,
            model: device.model,
            localizedModel: device.localizedModel,
            userInterfaceIdiom: getInterfaceIdiom(idiom: device.userInterfaceIdiom),
            identifierForVendor: device.identifierForVendor?.uuidString ?? "",
            orientation: getDeviceOrientation(orientation: device.orientation),
            isPortrait: device.orientation.isPortrait,
            isLandscape: device.orientation.isLandscape,
            isFlat: device.orientation.isFlat,
            batteryLevel: Double(device.batteryLevel),
            isBatteryMonitoringEnabled: device.isBatteryMonitoringEnabled,
            batteryState: getBatteryState(batteryState: device.batteryState),
            isProximityMonitorEnabled: device.isProximityMonitoringEnabled,
            proximityState: device.proximityState
        )
    }
    
    public func createTrackingDeviceInfo() -> TrackingDeviceInfo {
        let device = UIDevice.current
        let screenSize = Size(width: Float(UIScreen.main.bounds.width), height: Float(UIScreen.main.bounds.height));
        return TrackingDeviceInfo(
            advertisingDeviceId: advertisingDeviceId,
            deviceId: "",
            osVersion: device.systemVersion,
            brand: "iPhone",
            model: device.model,
            screenSize: screenSize,
            batteryLevel: Double(device.batteryLevel),
            limitedAdTracking: advertisingDeviceId.isEmpty,
            jailBroken: jailBroken,
            iphoneInfo: createTrackingIphoneInfo()
        )
    }
    
    private func createTrackingConnection() -> TrackingConnection {
        TrackingConnection(
            ip: UtilNetwork.findAddress(),
            type: UtilNetwork.getNetworkType()
        )
    }
    
    private func getInterfaceIdiom(idiom: UIUserInterfaceIdiom) -> String {
        switch (idiom) {
        case UIUserInterfaceIdiom.carPlay:
            return "CAR_PLAY";
        case UIUserInterfaceIdiom.pad:
            return "PAD";
        case UIUserInterfaceIdiom.phone:
            return "PHONE";
        case UIUserInterfaceIdiom.tv:
            return "TV";
        case UIUserInterfaceIdiom.unspecified:
            return "UNKNOWN"
        @unknown default:
            return "UNKNOWN"
        }
    }
    
    private func getDeviceOrientation(orientation: UIDeviceOrientation) -> String {
        switch orientation {
        case UIDeviceOrientation.faceDown:
            return "FACE_DOWN";
        case UIDeviceOrientation.faceUp:
            return "FACE_UP";
        case UIDeviceOrientation.landscapeLeft:
            return "LANDSCAPE_LEFT";
        case UIDeviceOrientation.landscapeRight:
            return "LANDSCAPE_RIGHT";
        case UIDeviceOrientation.portrait:
            return "PORTRAIT";
        case UIDeviceOrientation.portraitUpsideDown:
            return "PORTRAIT_UPSIDE_DOWN";
        case UIDeviceOrientation.unknown:
            return "UNKNOWN";
        @unknown default:
            return "UNKNOWN"
        }
    }
    
    private func getBatteryState(batteryState: UIDevice.BatteryState) -> String {
        switch batteryState {
        case UIDevice.BatteryState.charging:
            return "CHARGING";
        case UIDevice.BatteryState.full:
            return "FULL";
        case UIDevice.BatteryState.unplugged:
            return "UNPLUGGED";
        case UIDevice.BatteryState.unknown:
            return "UNKNOWN";
        @unknown default:
            return "UNKNOWN"
        }
    }
    
    private static func isJailBroken() -> Bool {
        if (FileManager.default.fileExists(atPath: "/Applications/Cydia.app") ||
            FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            FileManager.default.fileExists(atPath: "/bin/bash") ||
            FileManager.default.fileExists(atPath: "/usr/sbin/sshd") ||
            FileManager.default.fileExists(atPath: "/etc/apt") ||
            FileManager.default.fileExists(atPath: "/private/var/lib/apt/")) {
            return true
        }
        return false;
    }
}
