import Foundation

import SQLite3
import os.log

public class EventsTable {
    static let createEventsTableSQL = """
                                           CREATE TABLE IF NOT EXISTS events(
                                           id TEXT,
                                           attributes TEXT,
                                           timestamp INTEGER);
                                       """
    static let dropEventsTableSQL = "DROP TABLE events;"
    private let insertEventsSQL = "INSERT INTO events (id, attributes, timestamp) VALUES (?, ?, ?);"
    private let queryEventsSQL = "SELECT * FROM events LIMIT %d;"
    private let deleteEventsByKeySQL = "DELETE FROM events WHERE id IN (%@);"
    private let deleteOldEventsSQL = "DELETE FROM events WHERE id IN (SELECT id FROM events ORDER BY timestamp ASC LIMIT %d)"
    private let countEventsSQL = "SELECT COUNT(*) FROM events"
    private let analyticsDatabase: AnalyticsDatabase;
    
    init(database: AnalyticsDatabase) {
        self.analyticsDatabase = database;
    }
    
    public func insertEvent(event: TrackingEvent) -> Bool {
        guard let attributes = JsonEncoder.encode(event) else {
            return false;
        }

        var insertStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(insertStatement)
        }
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), insertEventsSQL, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (event.id as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, (attributes as NSString).utf8String, -1, nil)
            sqlite3_bind_int64(insertStatement, 3, DateTime.currentTimeMillis())
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                Logger.debug("Successfully saved event")
                return true;
            } else {
                let sqlError = String(cString: sqlite3_errmsg(insertStatement))
                Logger.debug(String(format:"Could not save event %@", sqlError))
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(insertStatement))
            Logger.debug(String(format:"INSERT statement could not be prepared %@", sqlError))
        }
        return false;
    }

    public func getEvents(limit: Int) -> [String: TrackingEvent] {
        var queryStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(queryStatement)
        }
        var events = [String: TrackingEvent]()
        var brokenIds = [String]()
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), String(format: queryEventsSQL, limit), -1, &queryStatement, nil) == SQLITE_OK {
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                let id = String(cString: sqlite3_column_text(queryStatement, 0))
                let attributes = String(cString: sqlite3_column_text(queryStatement, 1))
                guard let event = JsonEncoder.decode(TrackingEvent.self, from: attributes) else {
                    brokenIds.append(id)
                    continue;
                }
                events[id] = event
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(queryStatement))
            Logger.debug(String(format:"SELECT statement could not be prepared %@", sqlError))
        }

        if (!brokenIds.isEmpty) {
            let deletedKeys = clearByKeys(keys: brokenIds)
            Logger.debug(String(format:"Deleted '%d' broken keys", deletedKeys))
        }

        return events
    }

    public func countEvents() -> Int32 {
        var queryStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(queryStatement)
        }
        var count: Int32 = 0;
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), countEventsSQL, -1, &queryStatement, nil) == SQLITE_OK {
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                count = sqlite3_column_int(queryStatement, 0);
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(queryStatement))
            Logger.debug(String(format:"SELECT statement could not be prepared %@", sqlError))
        }
        return count
    }

    public func clearOldEvents(numberOfOldEvents: Int) -> Int32 {
        var deleteStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(deleteStatement)
        }
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), String(format: deleteOldEventsSQL, numberOfOldEvents), -1, &deleteStatement, nil) == SQLITE_OK {
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                return sqlite3_changes(analyticsDatabase.getConnection())
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(deleteStatement))
            Logger.debug(String(format:"DELETE statement could not be prepared %@", sqlError))
        }

        return 0
    }

    public func clearByKeys(keys: [String]) -> Int32 {
        let keysQuoted = keys.map {
            "'" + "\($0)" + "'"
        }
        var deleteStatement: OpaquePointer? = nil
        defer {
            sqlite3_finalize(deleteStatement)
        }
        if sqlite3_prepare_v2(analyticsDatabase.getConnection(), String(format: self.deleteEventsByKeySQL, keysQuoted.joined(separator: ",")), -1, &deleteStatement, nil) == SQLITE_OK {
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                Logger.debug(String(format:"Successfully deleted '%d' events", keys.count))
                return sqlite3_changes(analyticsDatabase.getConnection())
            } else {
                let sqlError = String(cString: sqlite3_errmsg(deleteStatement))
                Logger.debug(String(format:"Failed to delete '%d' events %@", keys.count, sqlError))
            }
        } else {
            let sqlError = String(cString: sqlite3_errmsg(deleteStatement))
            Logger.debug(String(format:"DELETE statement could not be prepared %@", sqlError))
        }
        return 0
    }
}
