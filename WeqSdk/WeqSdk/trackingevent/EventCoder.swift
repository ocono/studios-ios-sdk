import Foundation

class EventCoder {
    
    public static func unserializeTags(tags: Array<String>) -> Array<CustomTag> {
        var tagsInternal = Array<CustomTag>()
        for tag in tags {
            guard let tag = JsonEncoder.decode(CustomTag.self, from: tag) else {
                continue;
            }
            tagsInternal.append(tag)
        }
        return tagsInternal
    }
    
    public static func unserializeValue(value: String?) -> CustomValue? {
        if value == nil {
            return nil
        }
        
        if value!.isEmpty {
            return nil
        }
        
        guard let customValue = JsonEncoder.decode(CustomValue.self, from: value!) else {
            return nil
        }
        
        return customValue
    }
}
