import Foundation
import os.log
import Gzip

class EventPublisher {

    private let repository: EventRepository
    private let workerQueue: DispatchQueue = DispatchQueue(label: "weq-event-publisher")
    private let trackerState: TrackerState
    private let allowedFailedAttempts = 30
    private var failedAttempts = 0;
    private var startupDelayInSeconds = 10
    private var timer: DispatchSourceTimer
    private var httpApiReader: HttpApiReader

    init(repository: EventRepository, trackerState: TrackerState, httpApiReader: HttpApiReader) {
        self.repository = repository
        self.trackerState = trackerState;
        self.timer = DispatchSource.makeTimerSource(queue: workerQueue)
        self.httpApiReader = httpApiReader
        
        timer.schedule(
            deadline: .now() + DispatchTimeInterval.seconds(startupDelayInSeconds),
            repeating: DispatchTimeInterval.seconds(trackerState.sdkConfig.getEventFlushFrequencyInSeconds())
        )
    }

    public func start() {
        Logger.debug("Event publisher started")
        timer.setEventHandler {
            if self.shouldStop() == true {
                self.timer.cancel()
                Logger.debug("Terminating event publisher")
                return
            }
            
            if self.shouldSkip() == true {
                return
            }
            
            let events = self.repository.getEvents(limit: self.trackerState.sdkConfig.getEventFlushBatchSize())
            if events.count == 0 {
                return
            }

            let trackingEventBatch = self.createTrackingEventBatch(events)
            guard let jsonRequest = JsonEncoder.encode(trackingEventBatch) else {
                Logger.debug("Unable to decode event data, skipping")
                return
            }

            Logger.debug(String(format:"Sending batch of '%d' events", events.count))
            
            self.httpApiReader.sendPostBlocking(
                jsonRequest: jsonRequest,
                requestUri: "/event",
                successHandler: { response in
                   Logger.debug("Event batch request successfully sent.")
                   let ids = events.map { ($0.key) }
                   self.repository.clearByKeys(keys: ids)
                },
                errorHandler: {
                   self.failedAttempts += 1
                }
            )
        }
        
        timer.resume()
    }
    
    private func createTrackingEventBatch(_ events: [String: TrackingEvent]) -> TrackingEventBatch {
        return TrackingEventBatch(events: events.map {
            $0.value.session.gameUserId = self.trackerState.session.userId ?? ""
            return ($0.value)
        }, time: DateTime.now())
    }
    
    private func shouldStop() -> Bool {
        return self.failedAttempts >= self.allowedFailedAttempts
    }
    
    private func shouldSkip() -> Bool {
        if self.trackerState.session.userId == nil {
            return true
        }
        
        if !self.trackerState.isTrackingEnabled() {
            return true
        }
        
        if !UtilNetwork.isConnected() {
            return true
        }
        
        return false
    }
}
