import Foundation

public class TrackingEvent: Codable {
    var id: String
    var session: TrackingSession
    var event: WeQEvent
    var time: TrackingTime
    var gameId: String
    var appInfo: TrackingAppInfo
    var deviceInfo: TrackingDeviceInfo
    var connection: TrackingConnection

    init(id: String, session: TrackingSession, event: WeQEvent, time: TrackingTime, gameId: String, appInfo: TrackingAppInfo, deviceInfo: TrackingDeviceInfo, connection: TrackingConnection) {
        self.id = id
        self.session = session
        self.event = event
        self.time = time
        self.gameId = gameId
        self.appInfo = appInfo
        self.deviceInfo = deviceInfo
        self.connection = connection
    }
}
