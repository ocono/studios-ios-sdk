import Foundation

class TrackingIphoneInfo: Codable {
    var isMultitaskingSupported: Bool
    var name: String
    var systemName: String
    var systemVersion: String
    var model: String
    var localizedModel: String
    var userInterfaceIdiom: String
    var identifierForVendor: String
    var orientation: String
    var isPortrait: Bool
    var isLandscape: Bool
    var isFlat: Bool
    var batteryLevel: Double
    var isBatteryMonitoringEnabled: Bool
    var batteryState: String
    var isProximityMonitorEnabled: Bool
    var proximityState: Bool;

    init(isMultitaskingSupported: Bool, name: String, systemName: String, systemVersion: String, model: String, localizedModel: String, userInterfaceIdiom: String, identifierForVendor: String, orientation: String, isPortrait: Bool, isLandscape: Bool, isFlat: Bool, batteryLevel: Double, isBatteryMonitoringEnabled: Bool, batteryState: String, isProximityMonitorEnabled: Bool, proximityState: Bool) {
        self.isMultitaskingSupported = isMultitaskingSupported
        self.name = name
        self.systemName = systemName
        self.systemVersion = systemVersion
        self.model = model
        self.localizedModel = localizedModel
        self.userInterfaceIdiom = userInterfaceIdiom
        self.identifierForVendor = identifierForVendor
        self.orientation = orientation
        self.isPortrait = isPortrait
        self.isLandscape = isLandscape
        self.isFlat = isFlat
        self.batteryLevel = batteryLevel
        self.isBatteryMonitoringEnabled = isBatteryMonitoringEnabled
        self.batteryState = batteryState
        self.isProximityMonitorEnabled = isProximityMonitorEnabled
        self.proximityState = proximityState
    }
}
