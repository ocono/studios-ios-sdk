import Foundation

@objc public class CustomTag: NSObject, Codable {
    var name: String
    var value: String

    public init(name: String, value: String) {
        self.name = name;
        self.value =  value
    }
}
