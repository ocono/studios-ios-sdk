import Foundation

class TrackingConnection: Codable {
    var ip: String
    var type: ConnectionType

    init(ip: String, type: ConnectionType) {
        self.ip = ip
        self.type = type
    }
}
