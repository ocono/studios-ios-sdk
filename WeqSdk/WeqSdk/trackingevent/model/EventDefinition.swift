import Foundation

@objc public class EventDefinition: NSObject, Codable {
    var category: String
    var name: String
    var tags: Array<CustomTag>

    @objc public init(category: String, name: String, tags: Array<CustomTag>) {
        self.category = category
        self.name = name
        self.tags = tags
    }
}
