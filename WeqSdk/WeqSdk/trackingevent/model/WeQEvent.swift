import Foundation

@objc public class WeQEvent: NSObject, Codable {
    var type: EventDefinition
    var value: EventValue

    @objc public init(definition: EventDefinition, value: EventValue) {
        self.type = definition
        self.value = value
    }
}
