import Foundation

@objc public class EventValue: NSObject, Codable {
    var value1: CustomValue?
    var value2: CustomValue?
    var value3: CustomValue?
    var value4: CustomValue?
    var value5: CustomValue?

    @objc public init(value1: CustomValue?, value2: CustomValue?, value3: CustomValue?, value4: CustomValue?, value5: CustomValue?) {
        self.value1 = value1
        self.value2 = value2
        self.value3 = value3
        self.value4 = value4
        self.value5 = value5
    }
}
