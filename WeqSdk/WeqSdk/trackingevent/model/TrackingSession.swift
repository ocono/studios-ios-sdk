import Foundation

class TrackingSession: Codable {
    var id: String
    var advertisingDeviceId: String
    var gameUserId: String
    var startTime: String
    var currentTime: String
    var deviceId: String
    var gameCenterId: String

    init(id: String, advertisingDeviceId: String, gameUserId: String, startTime: String, currentTime: String, deviceId: String, gameCenterId: String) {
        self.id = id
        self.advertisingDeviceId = advertisingDeviceId
        self.gameUserId = gameUserId
        self.startTime = startTime
        self.currentTime = currentTime
        self.deviceId = deviceId
        self.gameCenterId = gameCenterId
    }
}
