import Foundation

public enum ConnectionType: String, Codable {
    case WIFI
    case CELLULAR_2_G
    case CELLULAR_3_G
    case CELLULAR_4_G
    case CELLULAR_5_G
    case UNKNOWN;
}
