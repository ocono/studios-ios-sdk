import Foundation

class TrackingAppInfo: Codable {
    var appId: String
    var build: String
    var version: String
    var signature: String

    init(appId: String, build: String, version: String, signature: String) {
        self.appId = appId
        self.build = build
        self.version = version
        self.signature = signature
    }
}
