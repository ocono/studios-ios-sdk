import Foundation

class TrackingTime: Codable{

    var clientTime: String

    init(clientTime: String) {
        self.clientTime = clientTime
    }
}
