import Foundation

class TrackingDeviceInfo: Codable {
    var advertisingDeviceId: String
    var deviceId: String
    var platform: String = "IOS"
    var osVersion: String
    var brand: String
    var model: String
    var screenSize: Size
    var batteryLevel: Double
    var limitedAdTracking: Bool
    var jailBroken: Bool
    var iphoneInfo: TrackingIphoneInfo

    init(advertisingDeviceId: String, deviceId: String, osVersion: String, brand: String, model: String, screenSize: Size, batteryLevel: Double, limitedAdTracking: Bool, jailBroken: Bool, iphoneInfo: TrackingIphoneInfo) {
        self.advertisingDeviceId = advertisingDeviceId
        self.deviceId = deviceId
        self.osVersion = osVersion
        self.brand = brand
        self.model = model
        self.screenSize = screenSize
        self.batteryLevel = batteryLevel
        self.limitedAdTracking = limitedAdTracking
        self.jailBroken = jailBroken
        self.iphoneInfo = iphoneInfo
    }
}
