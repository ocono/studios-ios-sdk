import Foundation

@objc public class CustomValue: NSObject, Codable {
    var value: String?
    var uom: String?

    public init(value: String, uom: String) {
        self.value = value;
        self.uom = uom;
    }

    public init(value: String) {
        self.value = value;
    }
}