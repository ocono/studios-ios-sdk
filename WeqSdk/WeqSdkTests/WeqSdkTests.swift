import XCTest
@testable import WeqSdkSimulator

class WeqSdkTests: XCTestCase {

    func testThatItSavesEvent() {
        let testSdkConfiguration = TestSdkConfiguration.create()
        let database = AnalyticsDatabaseFactory.create(dropBeforeCreating: true, databasePath: testSdkConfiguration.getDatabasePath())

        let eventsTable = EventsTable(database: database)
        let eventsBefore = eventsTable.getEvents(limit: 20)
        
        XCTAssertEqual(0, eventsBefore.count)

        var urls = [URL?: Data]()
        urls[URL(string: testSdkConfiguration.getEventIngressHostname() + "/event")] = "".data(using:.utf8)
        urls[URL(string: testSdkConfiguration.getEventIngressHostname() + "/game/game-id/user")] = "{\"advertiserDeviceId\": \"1\", \"userId\": \"user.1\"}".data(using:.utf8)
        
        WeqAnalytics.setInitialized(initialized: false)
        WeqAnalytics.setUrlSession(urlSession: UrlSessionMockFactory.create(urls: urls))
        WeqAnalytics.configure(
                config: WeQConfig(appToken: "app-token", gameId: "game-id"),
                sdkConfig: testSdkConfiguration
        )
        sleep(1)

        WeqAnalytics.event(
                event: WeQEvent(
                        definition: EventDefinition(category: "cat", name: "name", tags: []),
                        value: EventValue(
                                value1: CustomValue(value: "val1", uom: "uom"), 
                                value2: CustomValue(value: "val2", uom: "uom"), 
                                value3: CustomValue(value: "val3", uom: "uom"), 
                                value4: CustomValue(value: "val4", uom: "uom"), 
                                value5: CustomValue(value: "val5", uom: "uom")
                        )
                )
        )

        sleep(1)

        //Saved single event
        let numberOfEventsAfterTrackingEvent = eventsTable.countEvents()
        XCTAssertEqual(1, numberOfEventsAfterTrackingEvent)

        sleep(20)

        //Publisher flushes events to API, at this point there should be no events
        let numberOfEventsAfterFlush = eventsTable.countEvents()
        XCTAssertEqual(0, numberOfEventsAfterFlush)
    }

    func testThatItClearsOldEventsWhenLimitReached() {
        let testSdkConfiguration = TestSdkConfiguration.create(flushFrequencyInSeconds: "600000");
        let database = AnalyticsDatabaseFactory.create(dropBeforeCreating: true, databasePath: testSdkConfiguration.getDatabasePath())

        let eventsTable = EventsTable(database: database)
        let eventsBefore = eventsTable.getEvents(limit: 20)

        XCTAssertEqual(0, eventsBefore.count)
        
        var urls = [URL?: Data]()
        urls[URL(string: testSdkConfiguration.getEventIngressHostname() + "/event")] = "".data(using:.utf8)

        WeqAnalytics.setInitialized(initialized: false)
        WeqAnalytics.setUrlSession(urlSession: UrlSessionMockFactory.create(urls: urls))
        WeqAnalytics.configure(
                config: WeQConfig(appToken: "app-token", gameId: "game-id"),
                sdkConfig: testSdkConfiguration
        )

        sleep(1)

        let event = WeQEvent(
                definition: EventDefinition(category: "cat", name: "name", tags: []),
                value: EventValue(
                        value1: CustomValue(value: "val1", uom: "uom"),
                        value2: CustomValue(value: "val2", uom: "uom"),
                        value3: CustomValue(value: "val3", uom: "uom"),
                        value4: CustomValue(value: "val4", uom: "uom"),
                        value5: CustomValue(value: "val5", uom: "uom")
                )
        );

        var i = testSdkConfiguration.getEventStorageSizeLimit() + 5
        while (i > 0) {
            WeqAnalytics.event(event: event)
            i = i - 1;
        }

        sleep(1);

        //check that old events were cleared out when limit was reached, should be no more that limit
        let count = eventsTable.countEvents();
        XCTAssertEqual(testSdkConfiguration.getEventStorageSizeLimit(), Int(count))
    }

    func testThatItStopsTrackingWhenDisableRequested() {
        let testSdkConfiguration = TestSdkConfiguration.create(flushFrequencyInSeconds: "600000");
        let database = AnalyticsDatabaseFactory.create(dropBeforeCreating: true, databasePath: testSdkConfiguration.getDatabasePath())

        let eventsTable = EventsTable(database: database)
        let eventsBefore = eventsTable.getEvents(limit: 20)
        
        XCTAssertEqual(0, eventsBefore.count)

        var urls = [URL?: Data]()
        urls[URL(string: testSdkConfiguration.getEventIngressHostname() + "/event")] = "".data(using:.utf8)
        
        WeqAnalytics.setInitialized(initialized: false)
        WeqAnalytics.setUrlSession(urlSession: UrlSessionMockFactory.create(urls: urls))
        WeqAnalytics.configure(
                config: WeQConfig(appToken: "app-token", storeGameId: "game-id"),
                sdkConfig: testSdkConfiguration
        )
        sleep(1)

        WeqAnalytics.disableTracking();

        let event = WeQEvent(
                definition: EventDefinition(category: "cat", name: "name", tags: []),
                value: EventValue(
                        value1: CustomValue(value: "val1", uom: "uom"),
                        value2: CustomValue(value: "val2", uom: "uom"),
                        value3: CustomValue(value: "val3", uom: "uom"),
                        value4: CustomValue(value: "val4", uom: "uom"),
                        value5: CustomValue(value: "val5", uom: "uom")
                )
        );

        WeqAnalytics.event(event: event)

        sleep(1);

        let count = eventsTable.countEvents();
        XCTAssertEqual(0, Int(count))
    }
    
    func testThatItCollectsCorrectTrackingEvent() {
        let testSdkConfiguration = TestSdkConfiguration.create();
        let database = AnalyticsDatabaseFactory.create(dropBeforeCreating: true, databasePath: testSdkConfiguration.getDatabasePath())

        var urls = [URL?: Data]()
        urls[URL(string: testSdkConfiguration.getEventIngressHostname() + "/event")] = "".data(using:.utf8)
        
        WeqAnalytics.enableTracking();
        WeqAnalytics.setInitialized(initialized: false)
        WeqAnalytics.setUrlSession(urlSession: UrlSessionMockFactory.create(urls: urls))
        WeqAnalytics.configure(
                config: WeQConfig(appToken: "app-token", storeGameId: "game-id"),
                sdkConfig: testSdkConfiguration
        )
        
        sleep(1)
        
        var tags = Array<CustomTag>()
        tags.append(CustomTag(name: "tag-name1", value: "tag-value1"))
        tags.append(CustomTag(name: "tag-name2", value: "tag-value2"))
        
        let event = WeQEvent(
                definition: EventDefinition(category: "cat", name: "name", tags: tags),
                value: EventValue(
                        value1: CustomValue(value: "val1", uom: "uom1"),
                        value2: CustomValue(value: "val2", uom: "uom2"),
                        value3: CustomValue(value: "val3", uom: "uom3"),
                        value4: CustomValue(value: "val4", uom: "uom4"),
                        value5: CustomValue(value: "val5", uom: "uom5")
                )
        );

        WeqAnalytics.event(event: event)
        
        sleep(1);
        
        let eventsTable = EventsTable(database: database)
        var events = eventsTable.getEvents(limit: 1)
    
        XCTAssertEqual(1, events.count)
        
        let trackingEventMapEntry = events.popFirst()
        let trackingEvent = trackingEventMapEntry?.value
        
        XCTAssertNotNil(trackingEvent?.id)
        XCTAssertNotNil(trackingEvent?.time)
        XCTAssertEqual("game-id", trackingEvent?.storeGameId)
        XCTAssertEqual("cat", trackingEvent?.event.type.category)
        XCTAssertEqual("name", trackingEvent?.event.type.name)
        XCTAssertEqual(2, trackingEvent?.event.type.tags.count)
        
        XCTAssertEqual("tag-name1", trackingEvent?.event.type.tags[0].name)
        XCTAssertEqual("tag-value1", trackingEvent?.event.type.tags[0].value)
        
        XCTAssertEqual("tag-name2", trackingEvent?.event.type.tags[1].name)
        XCTAssertEqual("tag-value2", trackingEvent?.event.type.tags[1].value)
        
        //Session
        XCTAssertNotNil(trackingEvent?.session.id)
        XCTAssertEqual("00000000-0000-0000-0000-000000000000", trackingEvent?.session.advertisingDeviceId)
        XCTAssertEqual("", trackingEvent?.session.deviceId)
        XCTAssertNotNil(trackingEvent?.session.startTime)
        XCTAssertNotNil(trackingEvent?.session.currentTime)
        
        //Value
        XCTAssertEqual("val1", trackingEvent?.event.value.value1?.value)
        XCTAssertEqual("uom1", trackingEvent?.event.value.value1?.uom)
        XCTAssertEqual("val2", trackingEvent?.event.value.value2?.value)
        XCTAssertEqual("uom2", trackingEvent?.event.value.value2?.uom)
        XCTAssertEqual("val3", trackingEvent?.event.value.value3?.value)
        XCTAssertEqual("uom3", trackingEvent?.event.value.value3?.uom)
        XCTAssertEqual("val4", trackingEvent?.event.value.value4?.value)
        XCTAssertEqual("uom4", trackingEvent?.event.value.value4?.uom)
        XCTAssertEqual("val5", trackingEvent?.event.value.value5?.value)
        XCTAssertEqual("uom5", trackingEvent?.event.value.value5?.uom)
        
        //App info
        XCTAssertEqual("com.apple.dt.xctest.tool", trackingEvent?.appInfo.appId)
        XCTAssertEqual("15509", trackingEvent?.appInfo.build)
        XCTAssertEqual("", trackingEvent?.appInfo.version)
        XCTAssertEqual("", trackingEvent?.appInfo.signature)
        
        //Device info
        XCTAssertEqual("00000000-0000-0000-0000-000000000000", trackingEvent?.deviceInfo.advertisingDeviceId)
        XCTAssertEqual("", trackingEvent?.deviceInfo.deviceId)
        XCTAssertEqual("IOS", trackingEvent?.deviceInfo.platform)
        XCTAssertEqual("13.2.2", trackingEvent?.deviceInfo.osVersion)
        XCTAssertEqual("iPhone", trackingEvent?.deviceInfo.brand)
        XCTAssertEqual("iPhone", trackingEvent?.deviceInfo.model)
        XCTAssertEqual(375, trackingEvent?.deviceInfo.screenSize.width)
        XCTAssertEqual(812, trackingEvent?.deviceInfo.screenSize.height)
        XCTAssertEqual(-1, trackingEvent?.deviceInfo.batteryLevel)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.limitedAdTracking)
        XCTAssertEqual(true, trackingEvent?.deviceInfo.jailBroken)
        
        //Iphone info
        XCTAssertEqual(true, trackingEvent?.deviceInfo.iphoneInfo.isMultitaskingSupported)
        XCTAssertEqual("iPhone 11 Pro", trackingEvent?.deviceInfo.iphoneInfo.name)
        XCTAssertEqual("iOS", trackingEvent?.deviceInfo.iphoneInfo.systemName)
        XCTAssertEqual("13.2.2", trackingEvent?.deviceInfo.iphoneInfo.systemVersion)
        XCTAssertEqual("iPhone", trackingEvent?.deviceInfo.iphoneInfo.model)
        XCTAssertEqual("iPhone", trackingEvent?.deviceInfo.iphoneInfo.localizedModel)
        XCTAssertEqual("PHONE", trackingEvent?.deviceInfo.iphoneInfo.userInterfaceIdiom)
        XCTAssertNotNil(trackingEvent?.deviceInfo.iphoneInfo.identifierForVendor)
        XCTAssertEqual("UNKNOWN", trackingEvent?.deviceInfo.iphoneInfo.orientation)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.iphoneInfo.isPortrait)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.iphoneInfo.isLandscape)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.iphoneInfo.isFlat)
        XCTAssertEqual(-1, trackingEvent?.deviceInfo.iphoneInfo.batteryLevel)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.iphoneInfo.isBatteryMonitoringEnabled)
        XCTAssertEqual("UNKNOWN", trackingEvent?.deviceInfo.iphoneInfo.batteryState)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.iphoneInfo.isProximityMonitorEnabled)
        XCTAssertEqual(false, trackingEvent?.deviceInfo.iphoneInfo.proximityState)
        
        //Connection
        XCTAssertEqual("", trackingEvent?.connection.ip)
        XCTAssertEqual(ConnectionType.WIFI, trackingEvent?.connection.type)
    }
}


