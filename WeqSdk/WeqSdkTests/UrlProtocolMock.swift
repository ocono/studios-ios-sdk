import Foundation

class URLProtocolMock: URLProtocol {
    static var testURLs = [URL?: Data]()
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        if let url = request.url {
            if URLProtocolMock.testURLs[url] != nil {
                
                let response = HTTPURLResponse(
                    url: url,
                    statusCode: 200,
                    httpVersion: "2.0",
                    headerFields: [String: String]()
                )
                
                self.client?.urlProtocol(
                    self,
                    didReceive: response!,
                    cacheStoragePolicy: URLCache.StoragePolicy.allowed
                )
                
                if let data = URLProtocolMock.testURLs[url] {
                    self.client?.urlProtocol(
                        self,
                        didLoad: data
                    )
                }
            }
        }
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() { }
}

class UrlSessionMockFactory {
    
    public static func create(urls: [URL?: Data]) -> URLSession {
        URLProtocolMock.testURLs = urls;
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        
        return URLSession(configuration: config)
    }
}
