import Foundation

@testable import WeqSdkSimulator

class TestSdkConfiguration {

    public static func create() -> SdkConfiguration {
        TestSdkConfiguration.create(flushFrequencyInSeconds: "5")
    }

    public static func create(flushFrequencyInSeconds: String) -> SdkConfiguration {
        let sdkConfiguration = NSMutableDictionary()

        sdkConfiguration.setValue("http://localhost:8080", forKey: "WEQ_SDK_EVENT_INGRESS_HOSTNAME")
        sdkConfiguration.setValue(flushFrequencyInSeconds, forKey: "WEQ_SDK_EVENT_FLUSH_FREQUENCY_IN_SECONDS")
        sdkConfiguration.setValue("5", forKey: "WEQ_SDK_EVENT_FLUSH_BATCH_SIZE")
        sdkConfiguration.setValue("20", forKey: "WEQ_SDK_EVENT_STORAGE_SIZE_LIMIT")
        sdkConfiguration.setValue("5", forKey: "WEQ_SDK_EVENT_STORAGE_DELETE_OLDEST")
        sdkConfiguration.setValue("/tmp/analytics.sqlite", forKey: "WEQ_SDK_EVENT_DATABASE_PATH")

        return SdkConfiguration(applicationConfig: sdkConfiguration);
    }
}
