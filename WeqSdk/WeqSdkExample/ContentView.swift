import SwiftUI
import WeqSdkSimulator

struct ContentView: View {
    var body: some View {

        WeqAnalytics.configure(config: WeQConfig(appToken: "appToken", gameId: "gameId"))
        WeqAnalytics.debug(enable: true)

        var tags = Array<CustomTag>()
        tags.append(CustomTag(name: "tag-name", value: "tag-value"))

        return Button(action: {
            WeqAnalytics.event(
                event: WeQEvent(
                    definition: EventDefinition(category: "cat", name:"name", tags:tags),
                    value: EventValue(
                            value1: CustomValue(value: "val1", uom: "uom1"),
                            value2: CustomValue(value: "val2", uom: "uom2"),
                            value3: CustomValue(value: "val3", uom: "uom3"),
                            value4: CustomValue(value: "val4", uom: "uom4"),
                            value5: nil
                    )
                )
            )
            
            WeqAnalytics.registerParameter(name: "StringTest", type: "String", defaultValue: "Default", description: "My description")
            
            Logger.debug(WeqAnalytics.getParameter(name: "StringTest")!)
            
            
        }) {
            Text("Trigger tracking event")
                .fontWeight(.regular)
                .foregroundColor(Color.red)
        }
        .frame(width: 200.0, height: 50.0)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
