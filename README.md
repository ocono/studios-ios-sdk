# Summary

This is iOS SDK For WeQ Studios. It provides API to send data to WeQ Studios Analytics services.
The API only provides low level device specific methods. SDK integrates with WeQ Unity plugin.


The SDK is written in swift and build to .framework.Integrates with Cocoapods dependency manager.

For example to load library:

```
platform :ios, '10.0'
use_frameworks!

target 'Unity-iPhone' do
  pod 'WeqSdk', '~> 0.1'
end
```
